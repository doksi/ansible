# Doksi Ansible Collection

Ansible Collection for deploying and operating Doksi.

## Installing

To install this collection:

```shell
ansible-galaxy collection install ephracis.doksi
```

## Getting started

You can then use this collection in your playbooks:

```yaml
- name: Install Doksi using Podman
  hosts: all
  roles:
    - ephracis.doksi.podman
```

## Development

Before you start any development you should create a virtual environment:

```shell
python -m venv venv
```

Then whenever you want to start development you activate the environment:

```shell
source venv/bin/activate
```

After activation you must make sure you have all the requirements up to date:

```shell
dnf install -y python3-devel
pip install --upgrade pip
pip install -r requirements.txt
```

When you are finished with development you simply deactivate the environment:

```shell
deactivate
```

### Testing

We use Ansible Molecule to test this collection. To spin up virtual machines we
use Libvirt. This requires to you install libvirt on your machine:

```shell
dnf -y install libvirt
pip install libvirt-python
```

Create the instances:

```shell
molecule create
```

Run the playbooks:

```shell
molecule converge
```

Verify the result:

```shell
molecule verify
```

Destroy the instances:

```shell
molecule destroy
```

### Linting

Molecule is configured to run both `yamllint` and `ansible-lint`:

```shell
molecule lint
```

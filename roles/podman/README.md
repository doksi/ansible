# Ansible role ephracis.doksi.podman

Role for deploying Doksi with Podman.

## Installing

This role is part of the Doksi Ansible Collection. Install it with the
collection:

```shell
ansible-galaxy collection install ephracis.doksi
```

## Use role

You can then use this role in your playbook:

```yaml
- name: Install Doksi using Podman
  hosts: all
  roles:
    - ephracis.doksi.podman
```

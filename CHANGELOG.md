# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Upcoming]
### Added
- Configure the Doksi settings using the variable `doksi_settings`.
- Logs from Doksi are marked as such in the systemd journal.
- Output from the Doksi container are sent to the systemd journal for the
  service.

### Fixed
- The Doksi container was not able to start export jobs.

## [0.2.1]
### Fixed
- Podman role was missing a Readme.

## [0.2.0]
### Added
- Role for deploying with Podman.
- Readme instructions for using the collection.

## [0.1.1]
### Fixed
- The collection was not published to Galaxy.

## [0.1.0]
### Added
- Empty Ansible Collection.

[Upcoming]: https://gitlab.com/doksi/ansible/-/compare/0.2.1...master
[0.2.1]: https://gitlab.com/doksi/ansible/-/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/doksi/ansible/-/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/doksi/ansible/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/doksi/ansible/-/compare/f770f6cc...0.1.0
